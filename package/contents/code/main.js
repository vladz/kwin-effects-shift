/********************************************************************
 KWin - the KDE window manager
 This file is part of the KDE project.

 Copyright (C) 2018 Vlad Zagorodniy <vladzzag@gmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

"use strict";

var blacklist = [
    // The logout screen has to be animated only by the logout effect.
    "ksmserver ksmserver",
    "ksmserver-logout-greeter ksmserver-logout-greeter",

    // KDE Plasma splash screen has to be animated only by the login effect.
    "ksplashqml ksplashqml",
    "ksplashsimple ksplashsimple",
    "ksplashx ksplashx"
];

var shiftEffect = {
    loadConfig: function (window) {
        shiftEffect.duration = animationTime(160);
        shiftEffect.inScale = 0.95;
        shiftEffect.inOpacity = 0.0;
        shiftEffect.outScale = 0.95;
        shiftEffect.outOpacity = 0.0;
    },
    isShiftWindow: function (window) {
        // We don't want to animate most of plasmashell's windows, yet, some
        // of them we want to, for example, Task Manager Settings window.
        // The problem is that all those window share single window class.
        // So, the only way to decide whether a window should be animated is
        // to use a heuristic: if a window has decoration, then it's most
        // likely a dialog or a settings window so we have to animate it.
        if (window.windowClass == "plasmashell plasmashell"
                || window.windowClass == "plasmashell org.kde.plasmashell") {
            return window.hasDecoration;
        }

        if (blacklist.indexOf(window.windowClass) != -1) {
            return false;
        }

        if (window.hasDecoration) {
            return true;
        }

        // Don't animate combobox popups, tooltips, popup menus, etc.
        if (window.popupWindow) {
            return false;
        }

        // Override-redirect windows are usually used for user interface
        // concepts that are not expected to be animated by this effect.
        if (window.x11Client && !window.managed) {
            return false;
        }

        return window.normalWindow || window.dialog;
    },
    setupForcedRoles: function (window) {
        window.setData(Effect.WindowForceBackgroundContrastRole, true);
        window.setData(Effect.WindowForceBlurRole, true);
    },
    cleanupForcedRoles: function (window) {
        window.setData(Effect.WindowForceBackgroundContrastRole, null);
        window.setData(Effect.WindowForceBlurRole, null);
    },
    slotWindowAdded: function (window) {
        if (effects.hasActiveFullScreenEffect) {
            return;
        }
        if (!shiftEffect.isShiftWindow(window)) {
            return;
        }
        if (!window.visible) {
            return;
        }
        if (!effect.grab(window, Effect.WindowAddedGrabRole)) {
            return;
        }
        shiftEffect.setupForcedRoles(window);
        window.shiftInAnimation = animate({
            window: window,
            curve: QEasingCurve.InSine,
            duration: shiftEffect.duration,
            animations: [
                {
                    type: Effect.Scale,
                    from: shiftEffect.inScale
                },
                {
                    type: Effect.Translation,
                    from: {
                        value1: 0.0,
                        value2: 0.66 * window.height * (1 - shiftEffect.inScale)
                    }
                },
                {
                    type: Effect.Opacity,
                    from: shiftEffect.inOpacity
                }
            ]
        });
    },
    slotWindowClosed: function (window) {
        if (effects.hasActiveFullScreenEffect) {
            return;
        }
        if (!shiftEffect.isShiftWindow(window)) {
            return;
        }
        if (!window.visible) {
            return;
        }
        if (!effect.grab(window, Effect.WindowClosedGrabRole)) {
            return;
        }
        if (window.shiftInAnimation) {
            cancel(window.shiftInAnimation);
            delete window.shiftInAnimation;
        }
        shiftEffect.setupForcedRoles(window);
        window.shiftOutAnimation = animate({
            window: window,
            curve: QEasingCurve.OutSine,
            duration: shiftEffect.duration,
            animations: [
                {
                    type: Effect.Scale,
                    to: shiftEffect.outScale
                },
                {
                    type: Effect.Opacity,
                    to: shiftEffect.outOpacity
                }
            ]
        });
    },
    slotWindowDataChanged: function (window, role) {
        if (role == Effect.WindowAddedGrabRole) {
            if (window.shiftInAnimation && effect.isGrabbed(window, role)) {
                cancel(window.shiftInAnimation);
                delete window.shiftInAnimation;
                shiftEffect.cleanupForcedRoles(window);
            }
        } else if (role == Effect.WindowClosedGrabRole) {
            if (window.shiftOutAnimation && effect.isGrabbed(window, role)) {
                cancel(window.shiftOutAnimation);
                delete window.shiftOutAnimation;
                shiftEffect.cleanupForcedRoles(window);
            }
        }
    },
    init: function () {
        shiftEffect.loadConfig();

        effect.configChanged.connect(shiftEffect.loadConfig);
        effect.animationEnded.connect(shiftEffect.cleanupForcedRoles);
        effects.windowAdded.connect(shiftEffect.slotWindowAdded);
        effects.windowClosed.connect(shiftEffect.slotWindowClosed);
        effects.windowDataChanged.connect(shiftEffect.slotWindowDataChanged);
    }
};

shiftEffect.init();
